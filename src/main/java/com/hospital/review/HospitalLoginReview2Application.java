package com.hospital.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalLoginReview2Application {

    public static void main(String[] args) {
        SpringApplication.run(HospitalLoginReview2Application.class, args);
    }

}
